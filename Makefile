
export PRISMA_ENDPOINT=http://prisma:4466
export REACT_APP_API_ENDPOINT=http://localhost:4000/graphql

up:
	docker-compose -f "docker-compose.develop.yml" up -d --build

down:
	docker-compose -f "docker-compose.develop.yml" down

inapi:
	docker-compose -f "docker-compose.develop.yml" exec artland-api bash

infront:
	docker-compose -f "docker-compose.develop.yml" exec artland-front bash

up-staging:
	# cd front && make buildapp
	docker-compose -f "docker-compose.yml" up -d --build

down-staging:
	docker-compose -f "docker-compose.yml" down
