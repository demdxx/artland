# Artland test project

## Develop envorinment

1/ Run develop environment

```sh
make up
```

2/ Enter into the API project

```sh
make inapi

# root@31a8925a3295:/project#
# Apply migrations
make pdeploy

# Apply fixtures
make pseed

# Run API server
make app
```

3/ Enter into the front project

```sh
make infront

# Run front server
make app
```

4/ Open browser http://localhost:4001/

## Deploy staging

```sh
make up-staging
```
