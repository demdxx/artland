import { Prisma } from './generated/prisma-client'
import { ObjectStorage } from './storage';

export interface Context {
  prisma: Prisma
  storage: ObjectStorage
}
