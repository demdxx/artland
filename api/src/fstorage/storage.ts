
import { createWriteStream, unlink } from 'fs';
import * as mkdirp from 'mkdirp';
import * as shortid from 'shortid';

import { ObjectStorage } from '../storage';

export class FStorage implements ObjectStorage {
  constructor(protected directory = process.env.FSTORAGE_DIR || './upload') {
    // Ensure upload directory exists
    mkdirp.sync(directory);
  }

  protected async save(stream: any, filename: any): Promise<any> {
    const id   = shortid.generate();
    const path = `${this.directory}/${id}-${filename}`;

    console.log(
      id, path,
      this.preparePath(path),
    );
  
    return new Promise((resolve, reject) =>
      stream
        .pipe(createWriteStream(path))
        .on('finish', () => resolve({ id, path: this.preparePath(path) }))
        .on('error', reject),
    );
  }

  public async upload(upload: any): Promise<any> {
    const { stream, filename, _mimetype, _encoding } = await upload;
    return this.save(stream, filename);
  }

  public remove(path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      unlink(path, (err: any) => {
        if (err) reject(err);
        else resolve();
      });
    });
  }

  protected preparePath(path: string): string {
    path = path.replace(this.directory, "/upload");
    if (path.startsWith("./")) {
      return path.substr(2);
    }
    return path.replace(/^\/+/, "")
  }
};
