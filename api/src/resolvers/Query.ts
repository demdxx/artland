// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { QueryResolvers } from "../generated/graphqlgen";

export const Query: QueryResolvers.Type = {
  ...QueryResolvers.defaultResolvers,
  feed: (parent, {skip, after, before, first, last}, ctx) => {
    return ctx.prisma.photos({
      skip, after, before, first, last,
      orderBy: 'createdAt_DESC',
    })
  },

  filterPhoto: (parent, {searchString}, ctx) => {
    return ctx.prisma.photos({
      where: {
        title_contains: searchString,
      },
    })
  },

  photo: (parent, { id }, ctx) => {
    return ctx.prisma.photo({ id })
  }
};
