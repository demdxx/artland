// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { MutationResolvers } from "../generated/graphqlgen";
import { Artist } from "../generated/prisma-client";

export const Mutation: MutationResolvers.Type = {
  ...MutationResolvers.defaultResolvers,
  createPhoto: async (_obj, {file, title, username}, ctx) => {
    return ctx.prisma.upsertArtist({
      where: {username: username},
      create: {username: username},
      update: {},
    }).then(async (_artist: Artist) => {
      return ctx.storage.upload(file).then(async ({_id, path}) => {
        return ctx.prisma.createPhoto({
          image: path,
          title: title,
          artist: {
            connect: { username: username },
          },
        }).catch((reason: any) => {
          console.log("createPhoto error " + reason);
          return ctx.storage.remove(path);
        });
      });
    });
  }
};
