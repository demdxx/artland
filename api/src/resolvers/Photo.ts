// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { PhotoResolvers } from "../generated/graphqlgen";

export const Photo: PhotoResolvers.Type = {
  ...PhotoResolvers.defaultResolvers,

  artist: ({ id }, args, ctx) => {
    return ctx.prisma.photo({ id }).artist()
  }
};
