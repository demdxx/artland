
export interface ObjectStorage {
  upload(upload: any): Promise<any>;
  remove(path: string): Promise<any>;
}
