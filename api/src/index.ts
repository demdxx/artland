import { GraphQLServer } from 'graphql-yoga'
import { prisma }        from './generated/prisma-client'
import { resolvers }     from './resolvers'
import { FStorage }      from './fstorage'
import * as express      from 'express';

const port          = process.env.PORT || '4000';
const uploadDir     = process.env.FSTORAGE_DIR || './upload';

const server    = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: {
    prisma,
    storage: new FStorage(uploadDir),
  },
} as any)

server.express.use("/upload", express.static(uploadDir, {cacheControl: true, immutable: true}));

server.start({
  port: port,
  endpoint: '/graphql',
  subscriptions: '/subscriptions',
  playground: '/playground',
  cors: {
    origin:         "*",
    methods:        ['GET','PUT','POST','OPTIONS'],
    allowedHeaders: [
      'Accept',
      'Content-Type',
      'Authorization',
      'X-Real-Ip',
      'X-Forwarded-For',
      'User-Agent',
      'Referer',
    ],
  },
}, () => console.log(`Server is running on http://localhost:${port}`))