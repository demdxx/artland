FROM fnichol/uhttpd

LABEL maintainer="Dmitry Ponomarev <demdxx@gmail.com>"
LABEL service.name="artland-front"
LABEL service.weight=1
LABEL service.public="true"

ADD ./build /www
