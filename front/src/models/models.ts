
export interface IArtist {
  username: string;
}

export interface IItem {
  title: string;
  image: string;
  artist: IArtist;
}
