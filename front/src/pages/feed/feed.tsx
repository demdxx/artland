import * as React from 'react';
import { graphql } from 'react-apollo';
import { storage } from '../../storage';
import { IItem } from '../../models';

import gql from 'graphql-tag';

import './feed.scss';

const listPageSize = 5;

export interface IDatastore {
  addNew(item: IItem): void;
}

interface IProps {
  data?: any;
  onInit? (store: IDatastore): void;
}

class Feed extends React.Component<IProps> {
  protected finishLoad = false;

  public componentDidMount() {
    if (this.props.onInit) {
      this.props.onInit(this);
    }
    window.addEventListener("scroll", this.handleOnScroll);
  }

  public componentWillUnmount() {
    window.removeEventListener("scroll", this.handleOnScroll);
  }

  public addNew(item: IItem): void {
    this.props.data.feed.unshift(item);
    this.setState({}); // Refresh feed
  }

  public render() {
    return (<div className="feed">
      {this.props.data.feed && this.props.data.feed.map((item: any, index: any) => {
        return <div key={index} className="card">
          <span className="badge badge-success top">{item.artist.username}</span>
          <img className="card-img-top" src={storage.prepare(item.image)} alt={item.title} />
          <div className="card-body">
            <h5 className="card-title">
              {item.title}
            </h5>
          </div>
        </div>
      })}
      {this.props.data.loading && <div className="loader">
        <div className="lds-ripple"><div /><div /></div>
      </div>}
    </div>)
  }

  protected handleOnScroll = () => {
    if (this.finishLoad) {
      return;
    }

    // http://stackoverflow.com/questions/9439725/javascript-how-to-detect-if-browser-window-is-scrolled-to-bottom
    const scrollTop =
      (document.documentElement && document.documentElement.scrollTop) ||
      document.body.scrollTop;
    const scrollHeight =
      (document.documentElement && document.documentElement.scrollHeight) ||
      document.body.scrollHeight;
    const clientHeight =
      document.documentElement.clientHeight || window.innerHeight;

    // Can we load more?
    if (!this.props.data.loading && Math.ceil(scrollTop + clientHeight) >= scrollHeight) {
      this.props.data.loading = true;
      const lastID = this.lastItemID();

      this.props.data.fetchMore({
        variables: { cursor: lastID },
        updateQuery: this.updateQuery,
      })
    }
  }

  protected updateQuery = (previousResult: any, {fetchMoreResult}: any) => {
    if (!fetchMoreResult) {
      this.finishLoad = true;
      return previousResult;
    }

    this.finishLoad = fetchMoreResult.feed.length < listPageSize;

    return  {
      feed: [
        ...previousResult.feed,
        ...fetchMoreResult.feed,
      ]
    }
  }

  protected lastItemID(): string | null {
    let lastID: string | null = null;
    if (this.props.data.feed) {
      this.props.data.feed!.map((item: any) => { lastID = item.id + ''; });
    }
    return lastID;
  }
};

const ITEMS_QUERY = gql`
  query ($cursor: String) {
    feed (after: $cursor, first: ${listPageSize}) {
      id
      title
      image
      createdAt
      artist {
        username
      }
    }
  }
`

export default graphql<IProps>(ITEMS_QUERY)(Feed);