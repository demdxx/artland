
import { createStore, combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr';

const reducers = combineReducers({ toastr: toastrReducer });
const store = createStore(reducers);

export default store;
