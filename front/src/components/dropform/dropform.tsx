import * as React from 'react';
import * as PropTypes from 'prop-types';

import Dropzone from 'react-dropzone';
import { Mutation, MutationFn } from 'react-apollo';
import { OperationVariables } from 'apollo-client';
import gql from 'graphql-tag';
import { toastr } from 'react-redux-toastr';

import { IItem } from '../../models';

import './dropform.scss';

const UPLOAD_FILE = gql`
  mutation($file: Upload!, $title: String!, $username: String!) {
    createPhoto(file: $file, title: $title, username: $username) {
      id
      title
      image
      createdAt
      artist {
        username
      }
    }
  }`;

const overlayStyle: React.CSSProperties = {
  position:   'absolute',
  top:        0,
  right:      0,
  bottom:     0,
  left:       0,
  padding:    '2.5em 0',
  background: 'rgba(0,0,0,0.5)',
  textAlign:  'center',
  color:      '#fff',
  fontSize:   '2em',
};

interface IDropFormState {
  valid:    boolean;
  upload:   boolean;
  file:     any;
  image:    any;
  username: string;
  title:    string;
}

interface IDropFormProp {
  onUpload?(data: IItem | null, err: any): void;
}

class DropForm extends React.Component<IDropFormProp, IDropFormState> {
  public static contextTypes = {
    client: PropTypes.object
  }

  public username: string;
  public title: string;
  public mutation: MutationFn<any, OperationVariables>;

  constructor(props: any) {
    super(props);
    this.state = {
      valid: false,
      upload: false,
      file: null,
      image: '',
      username: '',
      title: '',
    };
  }

  public onDrop = (mutation: any) => (files: any) => {
    files.map((file: any) => {
      const reader = new FileReader();
      reader.onloadend = (e: any) => {
        this.mutation = mutation;
        this.setState({ valid: this.isValid(), file, image: e.target.result });
      };
      reader.readAsDataURL(file);
    });
  }

  public onChangeUsername = (event: any) => {
    this.setState({username: event.target.value});
    this.onValidate();
  }

  public onChangeTitle = (event: any) => {
    this.setState({title: event.target.value});
    this.onValidate();
  }

  public onUpload = async (event: any) => {
    if (this.isValid()) {
      // Call graphql API
      this.mutation({
        variables: { file: this.state.file, title: this.state.title, username: this.state.username },
        fetchPolicy: 'no-cache',
      }).then(data => {
        if (this.props.onUpload) {
          this.props.onUpload({image: this.state.image, title: this.state.title, artist: { username: this.state.username }}, null);
        }
        toastr.success("success", `Upload new image "${this.state.title}"`);
        this.onCancel();
      }).catch(err => {
        console.log("Upload file error", err);
        if (this.props.onUpload) {
          this.props.onUpload(null, err);
        }
        toastr.error(`Upload image "${this.state.title}" error`, err);
        this.onCancel();
      });
    }
    return;
  }

  public onValidate() {
    this.setState({valid: this.isValid()});
  }

  public onCancel = () => {
    this.setState({valid: false, username: '', title: '', upload: false, image: '', file: null});
  }

  public isValid(): boolean {
    return (this.state.username || '').trim() && (this.state.title || '').trim() && this.state.file;
  }

  public render() {
    return (
      <Mutation mutation={UPLOAD_FILE} fetchPolicy="no-cache">
        { (mutation, { loading }) => (
          <React.Fragment>
            <Dropzone
              accept={"image/png,image/jpeg,image/gif"}
              onDrop={this.onDrop(mutation)}
              multiple={false}
            >
              {({getRootProps, getInputProps, isDragActive}) => (
                !this.state.file ?
                  <div className="container">
                    <div {...getRootProps()} style={{position: "relative"}}>
                      <div className="uploadform">
                        <input {...getInputProps()} />
                        <h1 className="drop-title">Drop file here...</h1>
                        { isDragActive && <div style={overlayStyle}>Drop files here...</div> }
                      </div>
                    </div>
                  </div> : 
                  <div className="container">
                    <div className="uploadform">
                      <div className="row">
                        <div className={this.state.file ? 'col-4 image-block' : 'col-12'}>
                          <div {...getRootProps()} style={{position: "relative"}}>
                            <input {...getInputProps()} />
                            { isDragActive && <div style={overlayStyle}>Drop files here...</div> }
                            <img className="upload-image" src={this.state.image} />
                          </div>
                        </div>
                        {this.renderForm()}
                      </div>
                    </div>
                  </div>
              )}
            </Dropzone>
            { this.props.children }
          </React.Fragment>
        ) }
      </Mutation>
    );
  }

  protected renderForm() {
    return <div className="col-8">
      <div className="form-group">
        <label htmlFor="inputUsername">Username</label>
        <input type="text" className="form-control" id="inputUsername"
          placeholder="Username" onChange={this.onChangeUsername}
          disabled={this.state.upload} value={this.state.username} />
      </div>
      <div className="form-group">
        <label htmlFor="inputTitle">Title</label>
        <input type="text" className="form-control" id="inputTitle"
          placeholder="Title of the picture..." onChange={this.onChangeTitle}
          disabled={this.state.upload} value={this.state.title} />
      </div>
      <button type="button" className="btn btn-primary" onClick={this.onUpload}
        disabled={!this.state.valid || this.state.upload}>Upload</button>&nbsp;
      <button type="button" className="btn btn-default" onClick={this.onCancel}
        disabled={this.state.upload}>Cancel</button>
    </div>
  }
}

export default DropForm;
