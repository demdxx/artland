import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { storage } from './storage';

import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-client-preset';
import { ApolloProvider }   from 'react-apollo';
import { ApolloLink }       from 'apollo-link';
import { createUploadLink } from 'apollo-upload-client';
import ReduxToastr          from 'react-redux-toastr';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './context/store';

import './index.scss';

const uploadLink = createUploadLink({ uri: process.env.REACT_APP_API_ENDPOINT });
const httpLink = new HttpLink({ uri: process.env.REACT_APP_API_ENDPOINT });
storage.statichost  = process.env.REACT_APP_STATIC_HOST || 'http://localhost:4000';

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: ApolloLink.from([uploadLink, httpLink]),
})

ReactDOM.render(
  <ApolloProvider client={client}>
    <Provider store={store}>
      <ReduxToastr
        timeOut={4000}
        newestOnTop={true}
        preventDuplicates={true}
        position="top-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar={true}
        closeOnToastrClick={true} />
      <App />
    </Provider>
  </ApolloProvider>, 
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
