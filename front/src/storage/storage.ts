
export const storage = {
  statichost: "",

  prepare(path: string): string {
    if (path.match(/^(http[s]?:)?\/\//i) || path.match(/^data:image\//i)) {
      return path;
    }
    return this.statichost.replace(/\/+$/, "") + "/" + path.replace(/^\/+/, "");
  }
}

export default storage;
