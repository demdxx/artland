import * as React from 'react';

import Feed, { IDatastore } from './pages/feed';
import DropForm from './components/dropform';
import { IItem } from './models';

import './App.scss';

class App extends React.Component {
  protected store: IDatastore;

  public render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
          <a className="navbar-brand" href="#">Feed</a>
        </nav>
        <div className="main">
          <DropForm onUpload={this.onNew}>
            <div className="container">
              <Feed onInit={this.onFeedInit} />
            </div>
          </DropForm>
        </div>
      </React.Fragment>
    );
  }

  protected onNew = (data: IItem, err: any) => {
    if (data && !err) {
      this.store.addNew(data);
    }
  }

  protected onFeedInit = (store: IDatastore): void => {
    this.store = store;
  }
}

export default App;
